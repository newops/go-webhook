package main

import (
    "fmt"
    "net/http"
    "log"
    "os/exec"
)

func handler(w http.ResponseWriter, r *http.Request) {
    out, err := exec.Command("./deploy.sh").Output()
    if err != nil {
    	log.Fatal(err)
    }
    fmt.Fprintf(w, "%s triggered! \n%s", r.URL.Path[1:], out)
}

func main() {
    http.HandleFunc("/deploy", handler)
    http.ListenAndServe(":8008", nil)
}
